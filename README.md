# YTPMV Blocks
#### Copying
<p xmlns:dct="http://purl.org/dc/terms/" xmlns:vcard="http://www.w3.org/2001/vcard-rdf/3.0#">
  <a rel="license"
     href="http://creativecommons.org/publicdomain/zero/1.0/">
    <img src="http://i.creativecommons.org/p/zero/1.0/88x31.png" style="border-style: none;" alt="CC0" />
  </a>
  <br />
  To the extent possible under law,
  <a rel="dct:publisher"
     href="https://gitlab.com/Jayman2000/">
    <span property="dct:title">Jason Yundt</span></a>
  has waived all copyright and related or neighboring rights to
  <span property="dct:title">YTPMV Blocks</span>.
This work is published from:
<span property="vcard:Country" datatype="dct:ISO3166"
      content="US" about="https://gitlab.com/Jayman2000/">
  United States</span>.
</p>

Some parts of this mod were made by other people or weren't dedicated to the public domain using CC0. Here's a list of stuff that wasn't created by me and where it appears in this repository:
  * [CC0 software notice](https://wiki.creativecommons.org/wiki/CC0_FAQ#May_I_apply_CC0_to_computer_software.3F_If_so.2C_is_there_a_recommended_implementation.3F) by [Diane Peters](https://wiki.creativecommons.org/wiki/User:CCID-diane_peters), [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/).
    * A modified version of this notice is at the top of every souce code file.
  * [HTML CC0 waiver](https://creativecommons.org/choose/zero/results?license-class=zero&name=Jason+Yundt&actor_href=https%3A%2F%2Fgitlab.com%2FJayman2000%2F&work_title=YTPMV+Blocks&work_jurisdiction=US&confirm=confirm&understand=confirm&lang=en_US&field1=continue&waiver-affirm=affirm) by [Creative Commons](https://creativecommons.org), [CC BY 4.0](https://creativecommons.org/licenses/by/4.0).
    * This is the HTML placed at the top of the "Copying" section of README.md
  * [The CC0 1.0 Universal legal code as plaintext](https://creativecommons.org/publicdomain/zero/1.0/legalcode.txt) by [Creative Commons](https://creativecommons.org), [CC0](https://creativecommons.org/publicdomain/zero/1.0).
    * COPYING.txt
  * ["Fabric Example Mod"](https://github.com/FabricMC/fabric-example-mod/) by [FabricMC](https://fabricmc.net/), [CC0](https://creativecommons.org/publicdomain/zero/1.0).
    * This project was derrived from Fabric Example Mod. I used ["GeneratorFabricMod"](https://github.com/ExtraCrafTX/GeneratorFabricMod) by [ExtraCrafTX](https://github.com/ExtraCrafTX) to create the initial base for this project.
