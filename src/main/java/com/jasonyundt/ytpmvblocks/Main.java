/*
 * YTPMV Blocks - Adds more note blocks to Minecraft.
 *
 * Written in 2020 by Jason Yundt swagfortress@gmail.com
 *
 * To the extent possible under law, the author(s) have dedicated all copyright
 * and related and neighboring rights to this software to the public domain
 * worldwide. This software is distributed without any warranty.
 *
 * You should have received a copy of the CC0 Public Domain Dedication along
 * with this software. If not, see
 * <https://creativecommons.org/publicdomain/zero/1.0/>.
 */

package com.jasonyundt.ytpmvblocks;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.minecraft.block.Blocks;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public class Main implements ModInitializer {

	public static Logger LOGGER = LogManager.getLogger();

	public static final String MOD_ID = "jasonyundt_ytpmvblocks";
	public static final String MOD_NAME = "YTPMV Blocks";

	public static final StopSign STOP_SIGN;
	static {
		final var settings = new Item.Settings();
		STOP_SIGN = new StopSign(settings);
	}

	public static final StopSignNoteBlock STOP_SIGN_NOTE_BLOCK;
	static {
		final var settings = FabricBlockSettings.copyOf(Blocks.NOTE_BLOCK);
		STOP_SIGN_NOTE_BLOCK = new StopSignNoteBlock(settings);
	}

	public static final BlockItem STOP_SIGN_NOTE_BLOCK_ITEM;
	static {
		final var STOP_SIGN_NOTE_BLOCK_ITEM_SETTINGS = new Item.Settings();
		STOP_SIGN_NOTE_BLOCK_ITEM = new BlockItem(STOP_SIGN_NOTE_BLOCK, STOP_SIGN_NOTE_BLOCK_ITEM_SETTINGS);
	}

	@Override
	public void onInitialize() {
		log(Level.INFO, "Initializing");

		Registry.register(Registry.SOUND_EVENT, StopSign.OH.getId(), StopSign.OH);

		final var stopSignId = new Identifier(MOD_ID, "stop_sign");
		Registry.register(Registry.ITEM, stopSignId, STOP_SIGN);

		final var stopSignNoteBlockId = new Identifier(MOD_ID, "stop_sign_note_block");
		Registry.register(Registry.BLOCK, stopSignNoteBlockId, STOP_SIGN_NOTE_BLOCK);
		Registry.register(Registry.ITEM, stopSignNoteBlockId, STOP_SIGN_NOTE_BLOCK_ITEM);
	}

	public static void log(Level level, String message) {
		LOGGER.log(level, "[" + MOD_NAME + "] " + message);
	}

}