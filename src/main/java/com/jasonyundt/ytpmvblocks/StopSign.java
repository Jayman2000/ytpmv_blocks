/*
 * YTPMV Blocks - Adds more note blocks to Minecraft.
 *
 * Written in 2020 by Jason Yundt swagfortress@gmail.com
 *
 * To the extent possible under law, the author(s) have dedicated all copyright
 * and related and neighboring rights to this software to the public domain
 * worldwide. This software is distributed without any warranty.
 *
 * You should have received a copy of the CC0 Public Domain Dedication along
 * with this software. If not, see
 * <https://creativecommons.org/publicdomain/zero/1.0/>.
 */

package com.jasonyundt.ytpmvblocks;

import net.minecraft.block.BlockState;
import net.minecraft.item.Item;
import net.minecraft.item.ItemUsageContext;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvent;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Identifier;

public class StopSign extends Item {

	public static final SoundEvent OH;
	static {
		final var ID = new Identifier(Main.MOD_ID, "oh");
		OH = new SoundEvent(ID);
	}

	public StopSign(Settings settings) {
		super(settings);
	}

	@Override
	public ActionResult useOnBlock(ItemUsageContext context) {
		final var world = context.getWorld();
		if (!world.isClient) {
			final var blockPosition = context.getBlockPos();
			final var blockState = world.getBlockState(blockPosition);

			world.playSound(null, blockPosition, soundMadeBy(blockState), SoundCategory.RECORDS, 1.0f, 1.0f);
		}
		return ActionResult.SUCCESS;
	}

	public static SoundEvent soundMadeBy(BlockState blockState) {
		return OH;
	}
}