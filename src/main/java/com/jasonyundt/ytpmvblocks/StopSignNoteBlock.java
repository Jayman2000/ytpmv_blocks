/*
 * YTPMV Blocks - Adds more note blocks to Minecraft.
 *
 * Written in 2020 by Jason Yundt swagfortress@gmail.com
 *
 * To the extent possible under law, the author(s) have dedicated all copyright
 * and related and neighboring rights to this software to the public domain
 * worldwide. This software is distributed without any warranty.
 *
 * You should have received a copy of the CC0 Public Domain Dedication along
 * with this software. If not, see
 * <https://creativecommons.org/publicdomain/zero/1.0/>.
 */

package com.jasonyundt.ytpmvblocks;

import net.minecraft.block.BlockState;
import net.minecraft.block.NoteBlock;
import net.minecraft.particle.ParticleTypes;
import net.minecraft.sound.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class StopSignNoteBlock extends NoteBlock {

	public StopSignNoteBlock(Settings settings) {
		super(settings);
	}

	@Override
	public boolean onSyncedBlockEvent(BlockState state, World world, BlockPos pos, int type, int data) {
		final int noteNumber = state.get(NOTE);
		final float pitch = (float) Math.pow(2.0D, (noteNumber - 12) / 12.0D);

		world.playSound(null, pos, StopSign.OH, SoundCategory.RECORDS, 3.0F, pitch);
		world.addParticle(ParticleTypes.NOTE, pos.getX() + 0.5D, pos.getY() + 1.2D, pos.getZ() + 0.5D,
				noteNumber / 24.0D, 0.0D, 0.0D);
		return true;
	}
}
